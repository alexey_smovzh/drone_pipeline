// copy code to puppet server to environment 
// with same name as pushed branch
//
local puppet = {
  
  kind: "pipeline",
  type: "exec",
  name: "populate_code",

  clone: {
    disable: true,
  },

  trigger: {
    branch: "feature*",
  },

  steps: [
    {
      // ensure folder is empty and there are 
      // no artifacts from previous executions
      name: "delete_repository",
      commands: [
        "rm -rf /etc/puppetlabs/code/environments/${DRONE_BRANCH}",
      ],
    },
    {
      name: "clone_repository",
      commands: [
        "mkdir -p /etc/puppetlabs/code/environments/${DRONE_BRANCH}",
        "cd /etc/puppetlabs/code/environments/${DRONE_BRANCH}",
        "git clone ssh://git@gitea.west:2222/alex/drone-pipeline.git . -b ${DRONE_BRANCH} --depth 1",
      ],
    },
    {
      name: "validate_code",
      commands: [
        "cd /etc/puppetlabs/code/environments/${DRONE_BRANCH}",
        "/opt/puppetlabs/pdk/bin/pdk validate",
      ],
    },
  ],

  node: {
    "puppet-server-exec": "puppet-server.west",
  },
};


// run puppet agent on given node
// to apply pushed code to managed node
//
local deploy(label, host) = {

  kind: "pipeline",
  type: "exec",
  name: "deploy_" + host,

  depends_on: [
    "populate_code",
  ],

  clone: {
    disable: true,
  },

  trigger: {
    branch: "feature*",
  },

  steps: [
    {
      name: "run_puppet_agent",
      commands: [
        "bash -c 'sudo /opt/puppetlabs/bin/puppet agent --environment=${DRONE_BRANCH} -t || test $? -eq 2'",
      ],
    },
  ],

  node: {
    [label + "-exec"]: host,
  },
};


// main run 
[
  puppet,
  deploy("gitea", "gitea.west"),
  deploy("drone", "drone.west"),
  deploy("puppet-server", "puppet-server.west"),
]
