# Drone.io pipeline examples

Pipelines in this repository are for example purpose only.

In this example are described the following workflow:

- DevOps engineer commits code to Git repository
- Git trigger Drone pipeline
- Drone server execute pipeline
- Pipeline on first step instructs Puppet server to clone
  new code to Puppet environment with the same name as pushed branch has
- On next steps pipeline runs Puppet agent on managed nodes
  to apply new code to nodes configuration.


In this example are two kind of Drone pipelines.
Traditional Yaml example - '.drone.yml'
and Jsonnet scripted example - '.drone.jsonnet'

Drone server can use only one of this pipeline format
per repository at once. This formats can not be used 
simultaneously on one repository.

You must configure Drone which file to use in repository settings.

When you use jsonnet format for pipeline description. Drone server 
not execute it directly. It parse jsonnet and generate 'drone.yml' file. 
And then execute newly generated yml.

When you developing new pipeline with Jsonnet. You can test it
in any time with drone cli tool. Save your pipeline in current
directory with name '.drone.jsonnet' then execute command

```
  /opt/drone/drone jsonnet --stdout --stream
```


# Useful links

Jsonnet example: https://github.com/drone/drone-jsonnet-config/blob/master/.drone.jsonnet

